import { Component } from '@angular/core';
import { Egreso } from './egreso/egreso.model';
import { EgresoService } from './egreso/egreso.service';
import { Ingreso } from './ingreso/ingreso.model';
import { IngresoService } from './ingreso/ingreso.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  ingresos: Ingreso[] = [];
  egresos: Egreso[] = [];

  constructor(
    private ingresoService: IngresoService,
    private egresoService: EgresoService
  ) {
    this.ingresos = ingresoService.ingresos;
    this.egresos = egresoService.egresos;
  }

  getIngresosTotal() {
    let ingresosTotal: number = 0;

    this.ingresos.forEach((ingreso) => {
      ingresosTotal += ingreso.valor;
    });

    return ingresosTotal;
  }

  getEgresosTotal() {
    let egresosTotal: number = 0;

    this.egresos.forEach((egreso) => {
      egresosTotal += egreso.valor;
    });

    return egresosTotal;
  }

  getPorcentajeTotal() {
    return this.getEgresosTotal() / this.getIngresosTotal();
  }

  getPresupuestoTotal() {
    return this.getIngresosTotal() - this.getEgresosTotal();
  }
}
